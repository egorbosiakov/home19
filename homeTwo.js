(() => {
  let num = document.getElementById("inpNumb");
  num.oninput = function (e) {
    num = e.target.value;
    newNum = "";
    for (i = 0; i < num.length; i++) {
      if (isNaN(Number(num[i]))) {
        newNum = newNum + num[i];
      }
    }
    e.target.value = newNum;
  };
})();
